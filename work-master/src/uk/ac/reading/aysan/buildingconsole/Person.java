package uk.ac.reading.aysan.buildingconsole;

import java.awt.Point;

public class Person {
	
	private Point p;
	private Point doorLoc;
	
	public Person(int x, int y) {// where the person is in the room 
		p = new Point(); // intatiating p store the x and y
		p.setLocation(x, y);
	}
	
	public int getX() {
		return p.x;
	}
	
	public void setDoorLoc(Point p) {// location 
		doorLoc = p;
	}
	
	/*
	 * this is trying to get the person at the door
	 */
	public boolean movePerson() {
		if(!(p.x == doorLoc.x && p.y == doorLoc.y)) {// if the person is not exactly in the same spot as the door
			if(doorLoc.x > p.x)// translating gaining an x position 
				p.translate(1, 0);
			if(doorLoc.x < p.x) //loosing an x position
				p.translate(-1, 0);
			if(doorLoc.y > p.y)
				p.translate(0, 1);
			if(doorLoc.y < p.y)
				p.translate(0, -1);
			return true;
		} else {
			return false;
		}
	}
	
	public int getY() {
		return p.y;
	}
	
	public void setX(int x) {
		p.setLocation(x, p.y);// set to the new x keep y the same
	}
	
	public void setY(int y) {
		p.setLocation(p.x, y);
	}
	
	public void setPoint(int x, int y) {
		p.setLocation(x, y); // to set the entire location
	}	
	
	/**
	 * show person in the given building interface
	 * @param bi
	 */
	public void showPerson(BuildingInterface bi) {// asking for the building interface
		// call the showIt method in bi
		bi.showIt(p.x, p.y, 'p');//setting its position in the character array for the person
	}//bi building interface 

}
